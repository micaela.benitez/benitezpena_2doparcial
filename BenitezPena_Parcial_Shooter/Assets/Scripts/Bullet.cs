﻿using UnityEngine;

namespace BulletScript
{
    public class Bullet : MonoBehaviour
    {
        [Header("Bullet data")]
        public float speed = 50;
        public float timerActivated = 10;

        private Rigidbody rig;
        private float time;

        private void Start()
        {
            rig = GetComponent<Rigidbody>();
            rig.velocity = transform.TransformDirection(new Vector3(0, 0, speed));
        }

        private void Update()
        {
            time += Time.deltaTime;

            if (time > timerActivated)
                Destroy(gameObject);
                
        }
    }
}