﻿using System;
using UnityEngine;

namespace EnemyAIFSMScript
{
    public class EnemyAIFSM : MonoBehaviour
    {
        public static event Action<int> OnEnemyDied;   // Suma puntos al player
        public static event Action<int> OnGetLifePlayer;   // Saca vida al player

        [Header("Enemy data")]
        public int lives = 3;
        public int pointsDie = 10;   // Puntos que da al morir
        public int lifeLostPerShot = 1;   // Vida que pierde al ser disparado
        public int lifeTakeOutPlayer = 1;   // Vida que le saca al player

        public enum EnemyState
        {
            Idle,
            GoingToTarget,
            GoAway,
            Last
        }

        [Header("Enemy states data")]
        [SerializeField] private EnemyState state;
        public float speed = 6;
        public float targetDistance = 10;
        public float distanceToStop = 1;
        public float distanceToRestart = 5;
        public float timeStopped = 1;
        public Transform target;
        private float time;

        private void Update()
        {
            time += Time.deltaTime;

            switch (state)
            {
                case EnemyState.Idle:
                    if (time > timeStopped)
                    {
                        // Si el player se encuentra cerca del enemigo, este va a atacarlo
                        if (Vector3.Distance(target.transform.position, transform.position) < targetDistance)
                            NextState();
                    }
                    break;
                case EnemyState.GoingToTarget:
                    Vector3 dir = new Vector3(target.transform.position.x - transform.position.x, 0, target.transform.position.z - transform.position.z);
                    transform.Translate(dir.normalized * speed * Time.deltaTime, Space.World);

                    // Si el player se escapa, el enemigo vuelve a su comportamiento erratico
                    if (Vector3.Distance(target.transform.position, transform.position) > targetDistance)
                        state = EnemyState.Idle;

                    // Si toca al player le saca una vida y se aleja
                    if (Vector3.Distance(transform.position, target.transform.position) < distanceToStop)
                    {
                        OnGetLifePlayer?.Invoke(lifeTakeOutPlayer);
                        NextState();
                    }
                    break;
                case EnemyState.GoAway:
                    Vector3 dir02 = new Vector3(transform.position.x - target.transform.position.x, 0, transform.position.z - target.transform.position.z);
                    transform.Translate(dir02.normalized * speed * Time.deltaTime, Space.World);

                    // Cuando llego a cierto punto vuelve a su comportamiento erratico
                    if (Vector3.Distance(transform.position, target.transform.position) > distanceToRestart)
                        NextState();
                    break;
            }
        }

        private void NextState()
        {
            time = 0;
            int intState = (int)state;
            intState++;
            intState = intState % ((int)EnemyState.Last);
            SetState((EnemyState)intState);
        }

        private void SetState(EnemyState enemyState)
        {
            state = enemyState;
        }

        private void OnCollisionEnter(Collision collision)
        {
            if (collision.transform.tag == "Bullet")
            {
                lives -= lifeLostPerShot;

                if (lives <= 0)
                {
                    OnEnemyDied?.Invoke(pointsDie);
                    Destroy(gameObject);
                }
            }
        }
    }
}