using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameManagerScript;
using UnityEngine.Networking;
using LoaderManagerScript;
using UIMainMenuScript;
using UIRankingScript;
using UILoadingScreenScript;

namespace SqlConnectScript
{
    public class SqlConnect : MonoBehaviour
    {
        [NonSerialized] public List<string> allNames = new List<string>();
        [NonSerialized] public List<int> allScores = new List<int>();

        [NonSerialized] public bool incorrectPassword;
        [NonSerialized] public bool usernameExist;

        private void Start()
        {
            UIMainMenu.OnRegisterData += CallRegister;
            UIMainMenu.OnLoginData += CallLogin;
            GameManager.OnUpdateData += CallUpdate;
            UIRanking.OnRanking += CallRanking;
        }

        public void CallRegister()   // Llamada al registro, se llama al terminar los 2 minutos de juego
        {
            StartCoroutine(Register());
        }

        public void CallLogin()
        {
            StartCoroutine(Login());
        }

        public void CallUpdate()
        {
            StartCoroutine(UpdateBD());
        }

        public void CallRanking()
        {
            StartCoroutine(RankingName());
            StartCoroutine(RankingScore());
        }

        IEnumerator Register()
        {
            WWWForm form = new WWWForm();   // El wwwform lo que me permite es tratarlo como si fuera una URL, esto me permite guardar mis datos en un metodo get/post.
            form.AddField("name", GameManager.Get().playerName);   // Luego en el php hay que llamar el valor "Name" de la misma manera. Esto me busca el dato en el php con el nombre que le pasamos, y que dato voy a pasarle.
                                                                   // Como se ve internamente: Name = pepito (esto es un diccionario, le estoy dando un significado a mi valor).
            form.AddField("password", GameManager.Get().password);

            UnityWebRequest www = UnityWebRequest.Post("http://localhost/parcial/register.php", form);   // Esto me permite que me pueda conectar con una URL. // Tambien puede ser "http://127.0.0.1" (IP)
                                                                                                         // Cree la carpeta "parcial" dentro de la ruta que vimos antes en MAMB.
                                                                                                         // Ademas, le pasamos el dato form que es el dato que cree mas arriba.
                                                                                                         // Estoy generando un diccionario de form, form lo que me permite es guardar fiels con un significado y un valor.

            yield return www.SendWebRequest();   // Luego, hacemos que nos devuelva una variable de si fue cargado o no.
            switch(www.downloadHandler.text)
            {
                case "0":
                    usernameExist = false;
                    UILoadingScreen.Get().SetVisible(true);
                    LoaderManager.Get().LoadScene("Game");
                    break;
                case "1":
                    Debug.Log("Fallo la conexion");
                    break;
                case "2":
                    Debug.Log("Fallo la consulta");
                    break;
                case "3":
                    usernameExist = true;
                    incorrectPassword = false;
                    Debug.Log("El usuario con ese nombre ya existe");
                    break;
                default:
                    break;
            }
        }

        IEnumerator Login()
        {
            WWWForm form = new WWWForm();
            form.AddField("name", GameManager.Get().playerName);
            UnityWebRequest www = UnityWebRequest.Post("http://localhost/parcial/login.php", form);

            yield return www.SendWebRequest();

            if (www.result == UnityWebRequest.Result.Success)
            {
                string originalText = www.downloadHandler.text;
                string[] parts = originalText.Split('\t');   // Split separa el texto a partir de una concatenacion cada vez que encuentre un tab. En este caso es con \t
                GameManager.Get().playerName = parts[0];
                if (parts[1] == GameManager.Get().password)
                {
                    incorrectPassword = false;
                    UILoadingScreen.Get().SetVisible(true);
                    LoaderManager.Get().LoadScene("Game");
                }
                else
                {
                    incorrectPassword = true;
                    usernameExist = false;
                    Debug.Log("Contraseņa incorrecta");
                }
            }
        }               

        IEnumerator UpdateBD()
        {
            WWWForm form = new WWWForm();   // Primero hay que mandar un form, que es la coleccion de datos
            form.AddField("name", GameManager.Get().playerName);
            form.AddField("score", GameManager.Get().highscore);
            form.AddField("deaths", GameManager.Get().numberOfDeaths);
            UnityWebRequest www = UnityWebRequest.Post("http://localhost/parcial/update.php", form);   // Enviamos el form al script de php

            yield return www.SendWebRequest();

            if (www.result == UnityWebRequest.Result.Success)
            {
                LoaderManager.Get().LoadScene("Ranking");
            }
        }

        IEnumerator RankingName()
        {
            WWWForm form = new WWWForm();
            UnityWebRequest www = UnityWebRequest.Post("http://localhost/parcial/rankingname.php", form);

            yield return www.SendWebRequest();

            if (www.result == UnityWebRequest.Result.Success)
            {
                string originalText = www.downloadHandler.text;
                string[] parts = originalText.Split('\t');
                
                for (int i = 0; i < parts.Length - 1; i++)
                {
                    string username = parts[i];

                    allNames.Add(username);

                    Debug.Log(username);
                }    
            }
        }

        IEnumerator RankingScore()
        {
            WWWForm form = new WWWForm();
            UnityWebRequest www = UnityWebRequest.Post("http://localhost/parcial/rankingscore.php", form);

            yield return www.SendWebRequest();

            if (www.result == UnityWebRequest.Result.Success)
            {
                string originalText = www.downloadHandler.text;
                string[] parts = originalText.Split('\t');

                for (int i = 0; i < parts.Length - 1; i++)
                {
                    int score = int.Parse(parts[i]);

                    allScores.Add(score);

                    Debug.Log(score);
                }
            }
        }

        private void OnDisable()
        {
            UIMainMenu.OnRegisterData -= CallRegister;
            UIMainMenu.OnLoginData -= CallLogin;
            GameManager.OnUpdateData -= CallUpdate;
            UIRanking.OnRanking -= CallRanking;
        }
    }
}