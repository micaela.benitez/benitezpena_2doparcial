using UnityEngine;

namespace MouseLookScript
{
    public class MouseLook : MonoBehaviour
    {
        public float mouseSensitivity = 100.0f;
        public Transform playerBody;

        private float xRotation = 0.0f;
        private float minRotation = -90.0f;
        private float maxRotation = 90.0f;

        private void Start()
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }

        private void Update()
        {
            float mouseX = Input.GetAxis("Mouse X") * mouseSensitivity * Time.deltaTime;
            float mouseY = Input.GetAxis("Mouse Y") * mouseSensitivity * Time.deltaTime;

            xRotation -= mouseY;
            xRotation = Mathf.Clamp(xRotation, minRotation, maxRotation);

            transform.localRotation = Quaternion.Euler(xRotation, 0.0f, 0.0f);
            playerBody.Rotate(Vector3.up * mouseX);
        }
    }
}