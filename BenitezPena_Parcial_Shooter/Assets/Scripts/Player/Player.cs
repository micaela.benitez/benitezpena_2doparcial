using System;
using System.Collections;
using UnityEngine;
using EnemyAIFSMScript;
using GameManagerScript;
using Random = UnityEngine.Random;

namespace PlayerSript
{
    public class Player : MonoBehaviour
    {
        public float gravity = -20;

        [Header("Player data")]
        public CharacterController controller;
        public float speed = 10;
        public int maxLives = 3;
        //public float jumpHeight = 3;
        [NonSerialized] public int lives;
        [NonSerialized] public int points;

        private int speedModifier;
        private Vector3 velocity;

        private bool death;
        private float timeToRevive;

        [Header("Reviving canvas data")]
        public Canvas revivingScreen;

        [Header("Limits data")]
        public Transform minPosX;
        public Transform maxPosX;
        public Transform minPosZ;
        public Transform maxPosZ;

        private void Start()
        {
            death = false;
            lives = maxLives;
            points = 0;
            GameManager.Get().Init();

            EnemyAIFSM.OnEnemyDied += AddPoints;
            EnemyAIFSM.OnGetLifePlayer += LoseLife;
            CheckPointScript.CheckPoint.OnCheckPoint += CheckPoint;
        }

        private void Update()
        {
            if (!death)
            {
                float x = Input.GetAxis("Horizontal");
                float z = Input.GetAxis("Vertical");

                if (Input.GetKey(KeyCode.LeftShift))
                    speedModifier = 2;
                else
                    speedModifier = 1;

                Vector3 move = transform.right * x * speedModifier + transform.forward * z * speedModifier;

                controller.Move(move * speed * Time.deltaTime);

                //if (Input.GetKeyDown(KeyCode.Space))
                //    velocity.y = Mathf.Sqrt(jumpHeight * -2 * gravity);

                velocity.y += gravity * Time.deltaTime;

                controller.Move(velocity * Time.deltaTime);
            }
            else
            {
                timeToRevive += Time.deltaTime;
                StartCoroutine(CalculateRandomPosition());
            }
        }

        IEnumerator CalculateRandomPosition()
        {
            revivingScreen.gameObject.SetActive(true);

            yield return null;

            while (timeToRevive > 2)
            {
                GameManager.Get().UpdateNumberOfDeaths();
                points /= 2;
                lives = maxLives;
                transform.position = new Vector3(Random.Range(minPosX.position.x - 1, maxPosX.position.x), transform.position.y, Random.Range(minPosZ.position.z - 1, maxPosZ.position.z));

                death = false;
                timeToRevive = 0;

                revivingScreen.gameObject.SetActive(false);

                yield return null;
            }
        }

        private void AddPoints(int points)
        {
            this.points += points;
        }

        private void LoseLife(int amount)
        {
            lives -= amount;

            if (lives <= 0)
                death = true;
        }

        private void CheckPoint()
        {
            GameManager.Get().SaveData(points);
        }

        private void OnDisable()
        {
            EnemyAIFSM.OnEnemyDied -= AddPoints;
            EnemyAIFSM.OnGetLifePlayer -= LoseLife;
            CheckPointScript.CheckPoint.OnCheckPoint -= CheckPoint;
        }
    }
}