﻿using System;
using UnityEngine;
using MonoBehaviourSingletonScript;

namespace GameManagerScript
{
    public class GameManager : MonoBehaviourSingleton<GameManager>
    {
        public static event Action OnUpdateData;

        [NonSerialized] public string playerName = " ";
        [NonSerialized] public string password = " ";
        [NonSerialized] public int highscore = 0;
        [NonSerialized] public int numberOfDeaths = 0;

        [NonSerialized] public bool nameExist = false;
        [NonSerialized] public bool correctPassword = false;

        [NonSerialized] public float time;
        public float maxTimeInMinutes = 2;

        private bool gameIsOver = false;

        private void Update()
        {
            if (!gameIsOver)
                time += Time.deltaTime;

            if (time > (maxTimeInMinutes * 60))
            {
                OnUpdateData?.Invoke();
                gameIsOver = true;
                time = 0;
            }
        }

        public void Init()
        {
            gameIsOver = false;
            time = 0;
            numberOfDeaths = 0;
        }

        public void SaveData(int newHighscore)
        {
            highscore = newHighscore;
        }

        public void UpdateNumberOfDeaths()
        {
            numberOfDeaths++;
        }
    }
}