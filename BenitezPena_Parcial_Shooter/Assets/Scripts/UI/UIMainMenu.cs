﻿using System;
using UnityEngine;
using UnityEngine.UI;
using LoaderManagerScript;
using GameManagerScript;
using UILoadingScreenScript;
using TMPro;
using SqlConnectScript;

namespace UIMainMenuScript
{
    public class UIMainMenu : MonoBehaviour
    {
        [Header("Buttons canvas data")]
        public Canvas buttons;   

        [Header("Login canvas data")]
        public Canvas login;
        public InputField username;
        public InputField password;
        public TMP_Text incorrectPassword;
        public TMP_Text usernameExist;

        public static event Action OnRegisterData;
        public static event Action OnLoginData;

        private SqlConnect sql;

        private void Awake()
        {
            sql = FindObjectOfType<SqlConnect>();
        }

        private void Start()
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;

            buttons.gameObject.SetActive(true);
            login.gameObject.SetActive(false);
        }

        private void Update()
        {
            if (sql.gameObject != null)
            {
                if (sql.usernameExist)
                    usernameExist.gameObject.SetActive(true);
                else
                    usernameExist.gameObject.SetActive(false);

                if (sql.incorrectPassword)
                    incorrectPassword.gameObject.SetActive(true);
                else
                    incorrectPassword.gameObject.SetActive(false);
            }
        }

        public void LoadGameScene()
        {
            buttons.gameObject.SetActive(false);
            login.gameObject.SetActive(true);
        }

        public void LoadRankingScene()
        {
            UILoadingScreen.Get().SetVisible(true);
            LoaderManager.Get().LoadScene("Ranking");
        }

        public void LoadCreditsScene()
        {
            UILoadingScreen.Get().SetVisible(true);
            LoaderManager.Get().LoadScene("Credits");
        }

        public void LoginButton()
        {
            GameManager.Get().playerName = username.text;
            GameManager.Get().password = password.text;
            OnLoginData?.Invoke();
        }

        public void RegisterButton()
        {
            GameManager.Get().playerName = username.text;
            GameManager.Get().password = password.text;
            OnRegisterData?.Invoke();
        }

        public void ExitGame()
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#endif
            Application.Quit();
        }
    }
}