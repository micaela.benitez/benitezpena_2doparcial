﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;
using PlayerSript;
using LoaderManagerScript;
using GameManagerScript;
using UILoadingScreenScript;

namespace UIGameScript
{
    public class UIGame : MonoBehaviour
    {
        private Player player;

        [Header("Life bar data")]
        public Image lifeBar;
        private float minLife = 0;
        private float maxLife = 0;
        private float totalLive = 0;

        [Header("Points data")]
        public TMP_Text totalPoints;

        [Header("Timer data")]
        public TMP_Text timer;

        private void Start()
        {
            player = FindObjectOfType<Player>();
            maxLife = player.lives;
        }

        private void Update()
        {
            // Life bar
            totalLive = Mathf.Clamp(player.lives, minLife, maxLife);
            lifeBar.fillAmount = totalLive / maxLife;

            // Points
            totalPoints.text = "Points: " + player.points;

            // Timer
            timer.text = "" + GameManager.Get().time.ToString("F");

            // Para salir del juego
            //if (Input.GetKeyDown(KeyCode.Escape))
            //{
            //    UILoadingScreen.Get().SetVisible(true);
            //    LoaderManager.Get().LoadScene("MainMenu");
            //}
        }
    }
}