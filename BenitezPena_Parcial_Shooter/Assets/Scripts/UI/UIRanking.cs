using System;
using System.Collections.Generic;
using UnityEngine;
using LoaderManagerScript;
using UILoadingScreenScript;
using SqlConnectScript;
using TMPro;
using GameManagerScript;

namespace UIRankingScript
{
    public class UIRanking : MonoBehaviour
    {
        public List<TMP_Text> rankingNameData = new List<TMP_Text>();
        public List<TMP_Text> rankingScoreData = new List<TMP_Text>();
        public TMP_Text playerRanking;

        public static event Action OnRanking;

        private SqlConnect sql;

        private int topRanking = 10;

        private void Awake()
        {
            sql = FindObjectOfType<SqlConnect>();
        }

        private void Start()
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;

            OnRanking?.Invoke();
        }

        private void Update()
        {
            // Top 10 ranking
            if (sql.gameObject != null)
            {
                for (int i = 0; i < topRanking; i++)
                {
                    if (i < sql.allNames.Count)
                    {
                        rankingNameData[i].text = i + 1 + " -     " + sql.allNames[i];
                        rankingScoreData[i].text = "" + sql.allScores[i];
                    }
                }
            }

            // Posicion del jugador
            if (GameManager.Get().playerName != " ")
            {
                playerRanking.gameObject.SetActive(true);

                if (sql.gameObject != null)
                {
                    for (int i = 0; i < sql.allNames.Count; i++)
                    {
                        if (GameManager.Get().playerName == sql.allNames[i])
                            playerRanking.text = "You stayed in the position " + (i + 1) + ". Score: " + sql.allScores[i];
                    }
                }
            }
            else
            {
                playerRanking.gameObject.SetActive(false);
            }
        }

        public void LoadMainMenuScene()
        {
            UILoadingScreen.Get().SetVisible(true);
            LoaderManager.Get().LoadScene("MainMenu");
        }

        public void ExitGame()
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#endif
            Application.Quit();
        }
    }
}