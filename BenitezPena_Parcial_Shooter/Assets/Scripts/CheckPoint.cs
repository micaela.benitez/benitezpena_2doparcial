﻿using System;
using UnityEngine;

namespace CheckPointScript
{
    public class CheckPoint : MonoBehaviour
    {
        public static event Action OnCheckPoint;

        public ParticleSystem save;

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.tag == "Player")
            {
                Instantiate(save, transform.position, Quaternion.Euler(-90, 0, 0));
                OnCheckPoint?.Invoke();
            }
        }        
    }
}