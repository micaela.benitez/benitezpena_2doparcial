﻿using UnityEngine;
using BulletScript;

namespace WeaponScript
{
    public class Weapon : MonoBehaviour
    {
        [Header("Gun data")]
        public float shotMaxDistance = 5;

        [Header("Bullets data")]
        public Bullet bulletPrefab;

        private void Update()
        {
            Debug.DrawRay(transform.position, transform.forward * shotMaxDistance, Color.red);

            if (Input.GetMouseButtonDown(0))   // Disparar
            {
                Instantiate(bulletPrefab, transform.position, transform.rotation);
            }
        }
    }
}